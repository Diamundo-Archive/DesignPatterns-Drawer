package nl.kevinnauta.designpatternsdrawer;

public interface Command {
	/**
	 * Description: String that is added to TextLabel after undo/redo/do-ing this command.
	 */
	public default String getDescription() { return "Empty Command"; }
	
	public void execute();
	
	public void revive(); //opposite of execute
}
