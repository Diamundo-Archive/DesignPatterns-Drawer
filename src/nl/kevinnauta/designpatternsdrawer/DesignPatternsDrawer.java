package nl.kevinnauta.designpatternsdrawer;

import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class DesignPatternsDrawer {

    public static void main(String[] args) {
    	String time = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss").format( LocalDateTime.now() );
    	DEBUG.Print(time + ": Starting new instance of DesignPatternsDrawer.");

        DrawerFrame drawerFrame = new DrawerFrame(time);
    	
    	drawerFrame.addKeyListener(drawerFrame);
    	drawerFrame.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
            	drawerFrame.menuFileExit();
            }
        });
    	
        drawerFrame.addComponentListener( new ComponentAdapter() {
        	public void componentResized(ComponentEvent e) {
        		((DrawerFrame) e.getSource()).resizeFrame((int) e.getComponent().getBounds().getWidth(), (int) e.getComponent().getBounds().getHeight());
        	}
        });
        
    }
}
