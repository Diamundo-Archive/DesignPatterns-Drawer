package nl.kevinnauta.designpatternsdrawer;

import java.awt.Dimension;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

@SuppressWarnings("serial")
public class TextPanel extends JPanel{
    private JLabel textLabel;
    public void setTextLabel(String msg){ textLabel.setText(msg); }
    public void addTextLabel(String msg){ textLabel.setText(textLabel.getText() + " " + msg);}

    /**
     * Creating a status bar at the bottom which shows text to inform
     * the user about actions that are or should be performed
     * @param listener Reference to DrawerFrame
     */
    public TextPanel( DrawerFrame listener) {
        setSize(new Dimension( 800, 20 ));
        setPreferredSize(new Dimension(800, 20));
        setLayout(new BoxLayout(this, BoxLayout.X_AXIS));

        textLabel = new JLabel("I'm mr. Meeseeks! Look at me!");
        textLabel.setHorizontalAlignment(SwingConstants.LEFT);
        textLabel.setBorder(new EmptyBorder(0,10,0,0));
        add(textLabel);
    }
    
    public void resizeTextPanel(int newWidth, int newHeight) {
		this.setSize(new Dimension(newWidth, newHeight));
    	setPreferredSize(new Dimension(newWidth, newHeight));
    	revalidate();
    	repaint();
    }
    
}
