package nl.kevinnauta.designpatternsdrawer;

public class CommandUndoGroup implements Command {
    @Override
    public String getDescription() { return "Undo Group (" + fig.getID() + ") '" + (fig instanceof Group ? ((Group) fig).getFigures() : fig.getClass().getSimpleName() ) + "' to parent " + parentID; }

    private DrawerModel model; //pointer
    private DrawerModel snapshot;
    private Figure fig;
    private int parentID;

    /**
     * Command that makes a new group out of all items either within a rectangle or that have been ctrl+selected.
     * Rectangle and Ctrl+Selection are mutually exclusive!
     * @param model The model.
     * @param fig The group to disband, or the figure to move up one group.
     */
    public CommandUndoGroup(DrawerModel model, Figure fig) {
        this.snapshot = new DrawerModel(model.getListener());
        this.model = model;
        this.fig = fig;
        this.parentID = model.getParentOfFigure(fig.getID()).getID();
    }

    @Override
    public void execute() {
        snapshot.setFromClone(model);
        
        if( fig instanceof Group) { //disband group
	        for(Figure fig : ((Group) fig).getFigures()){
	            model.addFigure(fig, parentID);
	            model.addSelectID(fig.getID());
	        }
	    	model.removeItem( fig.getID() );
        } else { //move this figure to one group above
            model.removeItem(fig.getID());
        	model.addFigure(fig, model.getParentOfFigure(parentID).getID()); //get this figure's group's parent.
        	if( ((Group) model.getItemAtLocation(parentID)).getFigures().size() == 0) {
        		model.removeItem( parentID );
        	} //this group is now empty, remove it.
        }
    }

    @Override
    public void revive() {
        model.setFromClone(snapshot);
    }

}
