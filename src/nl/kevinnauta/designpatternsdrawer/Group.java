package nl.kevinnauta.designpatternsdrawer;

import java.awt.Point;
import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.Iterator;

public class Group extends Figure implements DrawableItem {
    private ArrayList<Figure> Figures;
    public  ArrayList<Figure> getFigures() { return Figures; }
    public  void setFigures ( ArrayList<Figure> newFigures ) { this.Figures = newFigures; }
    public  int getGroupSize() { return Figures.size(); }

    public Group(){
        this.setFigures( new ArrayList<>() );
    }

    public boolean addFigure(Figure newFigure, int groupID) {
		DEBUG.Print("GRP:24 Adding to group " + this.getID() + ", figure " + newFigure.toString(0));
    	if(this.getID() == groupID) {
    		this.Figures.add(newFigure);
    		return true;
    	} else {
    		//check subgroups
    		for(Figure fig : Figures) {
    			if(fig instanceof Group) {
    				if( ((Group) fig).addFigure(newFigure, groupID) ) {
    					return true;
    				}
    			}
    		}
    	}
    	return false;
    }

    @Override
    public Rectangle getSize() {
    	Rectangle r;
    	
    	if(getFigures().size() > 0) {
    		r = getFigures().get(0).getSize(); //set size to first figure's size.
    		
	    	for(int i = 1; i < getGroupSize(); i++) { //start at second figure, we've skipped the first already
	    		Figure fig = Figures.get(i);
	    		
	    		if( fig.getSize().getX() < r.getX()) {
	    			r.setSize((int) (r.getWidth() + ( r.getX() - fig.getSize().getX() )), (int) r.getHeight());
	    			r.setLocation( (int) fig.getSize().getX(), (int) r.getY() );
	    		}
	    		if( fig.getSize().getY() < r.getY()) {
	    			r.setSize((int) r.getWidth(), (int) (r.getHeight() + ( r.getY() - fig.getSize().getY() )));
	    			r.setLocation( (int) r.getX(), (int) fig.getSize().getY() );
	    		}
	    		if( fig.getSize().getX() + fig.getSize().getWidth() > r.getX() + r.getWidth()) {
	    			r.setSize( (int) ((fig.getSize().getX() + fig.getSize().getWidth()) - r.getX()), (int) r.getHeight() );
	    		}
	    		if( fig.getSize().getY() + fig.getSize().getHeight() > r.getY() + r.getHeight()) {
	    			r.setSize( (int) r.getWidth(), (int) ((fig.getSize().getY() + fig.getSize().getHeight()) - r.getY()) );
	    		}
	    	}

	    	//add borders of size 10 (additional 10 for width/height to counteract X/Y -10)
	    	r.setLocation((int) r.getX() - 10, (int) r.getY() - 10);
	    	r.setSize((int) r.getWidth() + 20, (int) r.getHeight() + 20);
	    	
    	} else {
    		if( this.getID() != 0) {
	    		DEBUG.Print("Empty group found, returning rectangle of 0's. " + this.getID());
	    		//DEBUG.Trace(this);
    		} else {
    			DEBUG.Print("Empty model found, returning rectangle of 0's.");
    		}
    		r = new Rectangle(0,0,0,0);
		}
    	return r;
    }
    
    /**
     * Function that fills a Group object, based on a set of Strings.
     * @param file The set of Strings to load this with
     * @param groupSize The amount of items to load here from the set of strings
     * @param ID The current maxID to give to children
     * @return The shortened version of input parameter file
     */
    public ArrayList<String> loadFromString(ArrayList<String> file, int groupSize, int ID) {
    	Iterator<String> lines;
    	String line;
    	String[] split;
		Figure newFig;
		int x;
		
		ID += 1;
		
		do { 
			lines = file.iterator(); //reset iterator every loop

    		line = lines.next();
    		lines.remove(); //pops the last item returned by next();
    		split = line.split(" ");
    		
    		switch(split[0]) {		
    			case "group" :
					newFig = new Group();
					file = ((Group) newFig).loadFromString(file, Integer.parseInt( split[1] ), ID);
					x = ID; //tmp storage of current ID
					ID = newFig.getID();
					newFig.setID(x);
					this.Figures.add(newFig);
					groupSize--;
					break;
				case "rectangle" :
				case "ellipse" :
					this.Figures.add( new BaseItem( line ).setID(ID) );
					groupSize--; ID++;
					break;
				case "ornament" :
					newFig = new Ornament();
					file = ((Ornament) newFig).loadFromString(file, split[1], split[2], ID);
					this.Figures.add(newFig);
					ID = newFig.getID()+1;
					groupSize--;
					break;
			} //END of switch
		} while( lines.hasNext() && groupSize > 0);

		this.setID(ID);
		return file;
    }
    
    public void removeFigure(int removeID) {
    	if(this.getID() == removeID){
    		this.Figures = new ArrayList<Figure>();
    		return;
    	}
    	
    	forloop:
    	for(Figure currentFigure : Figures) {
    		if( currentFigure.getID() == removeID || 
				(
					currentFigure instanceof Ornament && 
					((Ornament) currentFigure).getItemAtLocation(removeID) != null
    			) //TODO remove (this) if its figure waaaaay down is the figure to remove and it's only ornaments
			){
    			//if this is the item we want to remove, OR an ornament containing said figure, then remove it.
    			Figures.remove( currentFigure );
    			break forloop;
    		} else if( currentFigure instanceof Group ) {
    			((Group) currentFigure).removeFigure(removeID);
    		}
    	}
    }
    
    public void placeFigureAtLocation(Figure placeFigure, int oldID) {
    	forloop:
    	for(Figure currentFigure : Figures) {
			if (currentFigure.getID() == oldID) {
				this.Figures.set(Figures.indexOf(currentFigure), placeFigure);
				break forloop;
			} else if( currentFigure instanceof Group ) {
    			((Group) currentFigure).placeFigureAtLocation(placeFigure, oldID);
			} else if( currentFigure instanceof Ornament ) {
				((Ornament) currentFigure).placeFigureAtLocation(placeFigure, oldID);
			}
		}
	}

	public Figure getParentOfFigure(int childID){
		Figure parent;

		for(Figure fig : getFigures()){
    		DEBUG.Print("GetParent of "+ childID + " | this "+ fig.getClass().getSimpleName()+" ID=" + fig.getID());

    		if(fig.getID() == childID){
    			return this;
			} else if( fig instanceof Group) {
				parent = ((Group) fig).getParentOfFigure( childID );
				if(parent != null) { return parent; }
			} else if( fig instanceof Ornament ) {
				parent = ((Ornament) fig).getParentOfFigure( childID );
				if(parent != null) { return parent; }
			}
				
		}

		return null;
	}

	public Figure getItemAtLocation(int figureID) {
    	Figure currentFigure;
    	Figure foundItem = null;
    	Iterator<Figure> figures = Figures.iterator();

    	whileloop:
		while(figures.hasNext() && foundItem == null) {
    		currentFigure = figures.next();
    		if( currentFigure.getID() == figureID ) {
    			foundItem = currentFigure;
    			break whileloop;
			} else if( currentFigure instanceof Group ) {
				foundItem = ((Group) currentFigure).getItemAtLocation(figureID);
				if(foundItem != null) {
					break whileloop;
				}
			} else if( currentFigure instanceof Ornament ) {
				foundItem = ((Ornament) currentFigure).getItemAtLocation(figureID);
				if(foundItem != null) {
					break whileloop;
				}
			}
		}
		return foundItem;
	}

    public Figure getItemAtLocation(int x, int y){
    	Figure currentFigure;
    	Figure foundItem = null;
		Iterator<Figure> figures = Figures.iterator();
    	
		whileloop:
    	while(figures.hasNext() && foundItem == null) {
    		currentFigure = figures.next();
    		if( ( currentFigure instanceof BaseItem ) && currentFigure.getSize().contains(new Point(x,y)) ){ 
				foundItem = currentFigure; //this is the clicked item
    			break whileloop;
    		} else if ( currentFigure instanceof Ornament ) {
    			foundItem = ((Ornament) currentFigure).getItemAtLocation(x, y);
				if(foundItem != null) {
					break whileloop;
				}
    		} else if ( currentFigure instanceof Group) {
				foundItem = ((Group) currentFigure).getItemAtLocation(x, y);
				if(foundItem != null) {
					break whileloop;
				}
			}
    	}
		
		if(foundItem == null && this.getSize().contains(new Point(x,y))){
			foundItem = this;
		} //if it's not an item clicked, but a group, select this group.
		
    	return foundItem;
    }

    public boolean containsSelectedFigure( final ArrayList<Integer> selectedIDs ){
    	for(Figure fig : getFigures() ){
    		if( fig instanceof BaseItem && selectedIDs.contains(fig.getID())){
    			return true;
			} else if( fig instanceof Group ) {
    			return ((Group) fig).containsSelectedFigure(selectedIDs);
			} else if( fig instanceof Ornament ) {
    			return ((Ornament) fig).containsSelectedFigure(selectedIDs);
			}
		}
		return false;
	}

	public Group getGroupAtLocation(int clickX, int clickY) {
		Group tmp = null;
		
		forloop: //check all smaller groups first
		for(Figure fig : Figures){
			if(fig instanceof Group){
				tmp = ((Group) fig).getGroupAtLocation(clickX, clickY);
				if(tmp != null) {
					break forloop;
				}
			}
		} 
		
		//if no subgroup exists at (x,y), return this, else return null
		if(tmp == null && ( this.getSize().contains(new Point(clickX, clickY)) || this.getID() == 0)) {
			tmp = this;
		}

		return tmp;
	}

	/**
     * Prints this group.
     * @param tabs The number of tabs. Increases with each indentation level.
     */
    public String toString(int tabs){
    	String tmp = tabs(tabs) + "group " + (DEBUG.ID ? "(" + this.getID() + ") " : "") + getGroupSize() + "\n";
    	for(Figure fig : Figures) {
    		tmp += fig.toString( tabs+1 ) + "\n";
    	}
    	return tmp.substring(0, tmp.length()-1); // "\n" is length of 1, apparently
    }
    public String toString(){ return this.toString(0); }
}
