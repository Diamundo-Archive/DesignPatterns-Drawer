package nl.kevinnauta.designpatternsdrawer;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.Arrays;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JTextField;
import javax.swing.border.BevelBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

@SuppressWarnings("serial")
public class DrawerFrame extends JFrame implements KeyListener {

    private final DrawerMenuBar drawerMenuBar;
    private final DrawerPanel drawerPanel;
    private final TextPanel textPanel;

    public CommandStack commandStack;
    
    private DrawerModel drawerModel;
    public DrawerModel getModel() { return drawerModel; }
    public void setModel( DrawerModel newModel) { this.drawerModel = newModel; }
    
    public DrawerFrame(String time){
        super( (DEBUG.DEBUG ? time + " " : "") + "Design Patterns: Drawer!");
        drawerMenuBar = new DrawerMenuBar(this);
        this.setJMenuBar( drawerMenuBar );

        setSize(800, 600);
        setPreferredSize(new Dimension(800, 600));
        setResizable(true);
        setLocationRelativeTo(null);
        setVisible(true);
        setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE); 
        setLayout(new BorderLayout());
        
        drawerPanel = new DrawerPanel(this);
        drawerPanel.setBorder(new BevelBorder(BevelBorder.LOWERED));
        add(drawerPanel, BorderLayout.CENTER);
        
        textPanel = new TextPanel(this);
        textPanel.setBorder(new BevelBorder(BevelBorder.LOWERED));
        add(textPanel, BorderLayout.SOUTH);
        
        drawerModel = new DrawerModel(this);
        commandStack = new CommandStack(this);
        drawerMenuBar.onNotify(drawerModel); //disable selectID-based items
        
        revalidate();
        repaint();  
    }

    public void setText(String msg) { textPanel.setTextLabel(msg); DEBUG.Print(msg); }
    public void addText(String msg) { textPanel.addTextLabel(msg); DEBUG.Print(msg); }
    
    /**
     * Function that is called whenever the frame's dimension are changed
     * @param newWidth The new Width of the Frame.
     * @param newHeight The new Height of the Frame.
     */
    public void resizeFrame(int newWidth, int newHeight) {
    	DEBUG.Print("Resizing to: " + newWidth + " x " + newHeight + ".");
    	this.setSize(newWidth, newHeight);
    	drawerPanel.resizeDrawerPanel(newWidth, newHeight - 80);
    	textPanel.resizeTextPanel(newWidth, 20); 
    	this.revalidate();
    	this.repaint();
    	drawerPanel.onNotify(drawerModel);
    }
    
    /**
     * Function called whenever the DrawerModel updates.
     */
    public void onNotify() { 
    	drawerMenuBar.onNotify(drawerModel);
		drawerMenuBar.revalidate();
		drawerMenuBar.repaint();
        drawerPanel.onNotify(drawerModel);
		drawerPanel.revalidate();
		drawerPanel.repaint();
        revalidate();
        repaint();
    }
    
    /**
     * Function that aids the MenuBar File group, to check whether a currently loaded model has unsaved changes.
     */
    private void checkToSaveModel() {
    	DEBUG.Print("checkToSaveModel: " + getModel().isEdited());
        if( getModel().isEdited() ) {
            if( getModel().getFilePath().isEmpty() ) {
                menuFileSaveAs();
            } else {
                menuFileSave();
            }
            getModel().setEdited(false);
        }
    }
    
    /**
     * Function called, when the user wants to make a new model.
     */
    public void menuFileNew(){
    	DEBUG.Print("Menu New opened");
    	checkToSaveModel();
        commandStack.doCommand( new CommandNewModel(this.drawerModel) );
    }
    
    /**
     * Function called when the user wants to open a model from a saved file
     */
    public void menuFileOpen(){
    	DEBUG.Print("Menu Open opened");
    	checkToSaveModel();

        setText("Opening file...");
        JFileChooser chooser = new JFileChooser();
        FileNameExtensionFilter filter = new FileNameExtensionFilter(".txt files", "txt");
        chooser.setFileFilter(filter);

        int returnVal = chooser.showOpenDialog(null);
        if(returnVal == JFileChooser.APPROVE_OPTION) {
            // Clear current model, only then load new model
            addText("New File opened!");
            String filePath = chooser.getSelectedFile().getAbsolutePath();
            commandStack.doCommand( new CommandOpenFile(drawerModel, filePath) );
        } else {
            setText("Opening cancelled!");
        }
        revalidate();
        repaint();
    }
    
    /**
     * Function called when the user wants to save the model to a predetermined location
     */
    public void menuFileSave(){
    	DEBUG.Print("Menu Save opened");
        if(getModel().getFilePath().isEmpty() ) {
            menuFileSaveAs();
        } else {
            setText("Saving file...");
            getModel().save();
        }
    }
    
    /**
     * Function called when the user wants to save the model to a yet to specify location
     */
    public void menuFileSaveAs(){
    	DEBUG.Print("Menu Save As opened");
        setText("Saving file...");
        JFileChooser chooser = new JFileChooser();
        FileNameExtensionFilter filter = new FileNameExtensionFilter(".txt files", "txt");
        chooser.setFileFilter(filter);

        int returnVal = chooser.showSaveDialog(null);
        if(returnVal == JFileChooser.APPROVE_OPTION) {
            // Make sure the file will be properly saved with extension
            String path = chooser.getSelectedFile().getAbsolutePath();
            if(!path.endsWith(".txt")){
                path += ".txt";
            }
            getModel().setFilePath(path);
            getModel().save();
        } else {
            setText("Saving cancelled!");
        }
    }
    
    public void menuFileExit() {
    	DEBUG.Print("Menu Exit opened");
    	checkToSaveModel();
    	System.exit(0);
    }
    
    /**
     * Function called when the user wants to add a new item, but hasn't clicked in the panel and thus no coordinates.
     */
    public void menuEditNewItem() {
    	DEBUG.Print("Menu New Item () opened");
        final int defaultX = drawerPanel.getWidth()/3;
        final int defaultY = drawerPanel.getHeight()/3;
        menuEditNewItem(defaultX, defaultY);
    }
    
    /**
     * Function that will redirect to the JOptionPane to add a figure, when no figure has been selected
     * @param clickX The x-coordinate that was clicked.
     * @param clickY The y-coordinate that was clicked.
     */
    public void menuEditNewItem(final int clickX, final int clickY){
    	DEBUG.Print("Menu New Item (int, int) opened");
    	menuEditEditItem(new ArrayList<Integer>( Arrays.asList(0)), clickX, clickY, 0, 0, null);
    }
    
    /**
     * Function that will redirect to the JOptionPane to edit a figure, when a figure has been selected
     * @param selectedIDs The IDs of the selected figure.
     */
    public void menuEditEditItem(final ArrayList<Integer> selectedIDs){
    	DEBUG.Print("Menu Edit Item (int) opened");
    	menuEditEditItem(selectedIDs, 0, 0, 0, 0, null);
    }
    
    /**
     * Function that will redirect to the JOptionPane to handle a dragged size.
     * @param drawGroup The 
     */
    public void menuEditEditItem(final Rectangle drawGroup, final Point endpoint) {
		DEBUG.Print("Menu Edit Item (Rectangle, Point) opened");
    	menuEditEditItem( (int) drawGroup.getX(), (int) drawGroup.getY(), (int) drawGroup.getWidth(), (int) drawGroup.getHeight(), endpoint);
	}

    /**
     * Function that will redirect to the JOptionPane with four coordinates.
     * @param clickX Left-side X coordinate
     * @param clickY Top-side Y coordinate
     * @param clickW Right-side X coordinate
     * @param clickH Bottom-side Y coordinate
     */
	public void menuEditEditItem(final int clickX, final int clickY, final int clickW, final int clickH, final Point endpoint){
        DEBUG.Print("Menu Edit Item (int, int, int, int, Point) opened");
        menuEditEditItem( null, clickX, clickY, clickW, clickH, endpoint);
    }

    /**
     * Helper function to return the JOptionPane for which a given button is from.
     * @param parent Idunno
     * @return The JOptionPane to return.
     */
    protected JOptionPane getOptionPane(JComponent parent) {
        JOptionPane pane = null;
        if (!(parent instanceof JOptionPane)) {
            pane = getOptionPane((JComponent)parent.getParent());
        } else {
            pane = (JOptionPane) parent;
        }
        return pane;
    }
    
    /**
     * Function that shows the editscreen for either an existing item (selectedID), or to add a new item
     * Private, because only redirect-functions from DrawerFrame are allowed to redirect here.
     * @param selectedIDs The ID of the existing item to edit, if present.
     * @param clickX The x-coordinate that was clicked, if present.
     * @param clickY The y-coordinate that was clicked, if present.
     * @param clickW The width of the selected box, if present.
     * @param clickH The width of the selected box, if present.
     */
    private void menuEditEditItem( final ArrayList<Integer> selectedIDs, final int clickX, final int clickY, final int clickW, final int clickH, /* final */ Point endPoint) {
    	DEBUG.Print("Menu Edit Item (int, int, int, int, int) opened");
    	String messageTitle;
        Figure selectedFigure;
        int selectedID = 0;
        if(endPoint == null){ endPoint = new Point(0,0); } //make sure Point is filled, to collect a group.
    	
        ArrayList<String> typesList = new ArrayList<>();
        for(BaseItem.TYPE t : BaseItem.TYPE.values() ){
        	typesList.add( t.toString().substring(0,1).toUpperCase() + t.toString().substring(1, t.toString().length()).toLowerCase() );
        } // Capitalize the type
        JComboBox<String> typesField = new JComboBox<>( typesList.toArray(new String[0]) );
        JTextField startXField = new JTextField(); 
        JTextField startYField = new JTextField(); 
        JTextField  widthField = new JTextField();
        JTextField heightField = new JTextField();
        final JButton confirmButton = new JButton("Ok");
        final JButton cancelButton = new JButton("Cancel");

        if(selectedIDs != null && selectedIDs.size() == 1 && !selectedIDs.contains(0) ) { //Edit an item
            selectedID = selectedIDs.get(0);
        }

        if(selectedID != 0){
            messageTitle = "Edit this Figure:";
    		selectedFigure = getModel().getItemAtLocation(selectedIDs.get(0));
    		
    		if(selectedFigure instanceof BaseItem) {
	            startXField.setText( Integer.toString( ((BaseItem) selectedFigure).getLeft() ) );
	            startYField.setText( Integer.toString( ((BaseItem) selectedFigure).getTop() ) );
	             widthField.setText( Integer.toString( ((BaseItem) selectedFigure).getWidth() ) );
	            heightField.setText( Integer.toString( ((BaseItem) selectedFigure).getHeight() ) );
	        	
	            typesField.setSelectedItem( 
	            		((BaseItem) selectedFigure).getType().toString().substring(0,1).toUpperCase() 
	            		+ ((BaseItem) selectedFigure).getType().toString().substring(1, ((BaseItem) selectedFigure).getType().toString().length()).toLowerCase() );
    		}
    		
    	} else {
    		messageTitle = "Add a new Figure:";
    		selectedFigure = null;
    		
			startXField.setText(String.valueOf(clickX == 0 ? "" : clickX));
			startYField.setText(String.valueOf(clickY == 0 ? "" : clickY));
			 widthField.setText(String.valueOf(clickW == 0 ? "" : clickW));
			heightField.setText(String.valueOf(clickH == 0 ? "" : clickH));
			typesField.setSelectedItem("Rectangle");
    	}
    	
        //Used for allowing user to submit the optionpane or not
        confirmButton.addActionListener(new ActionListener() {
            @Override public void actionPerformed(ActionEvent e) {
                JOptionPane pane = getOptionPane((JComponent)e.getSource());
                pane.setValue(confirmButton);
            }
        });
        cancelButton.addActionListener(new ActionListener() {
            @Override public void actionPerformed(ActionEvent e) {
                JOptionPane pane = getOptionPane((JComponent)e.getSource());
                pane.setValue(cancelButton);
            }
        });
        
        //add documentlisteners
        DocumentListener isValidDL = new DocumentListener(){
            @Override public void changedUpdate(DocumentEvent de){ validate(de); }
            @Override public void removeUpdate (DocumentEvent de){ validate(de); }
            @Override public void insertUpdate (DocumentEvent de){ validate(de); }
            
            public void validate(DocumentEvent e) {
                try {
                    Integer.parseInt(startXField.getText());
                    Integer.parseInt(startYField.getText());
                    Integer.parseInt( widthField.getText());
                    Integer.parseInt(heightField.getText());
                    //TODO NICE Out of Range (offscreen)

                    //all four numbers are integers:
                    confirmButton.setEnabled(true);
                } catch (NumberFormatException nfe) { //NaN or empty
                    confirmButton.setEnabled(false);
                } 
            }
        };
        startXField.getDocument().addDocumentListener(isValidDL);
        startYField.getDocument().addDocumentListener(isValidDL);
         widthField.getDocument().addDocumentListener(isValidDL);
        heightField.getDocument().addDocumentListener(isValidDL);
        isValidDL.changedUpdate(null);
        
        //prepare JOptionPane
        Object[] options = {
            "Type", typesField,
            "Starting Position X", startXField,
            "Starting Position Y", startYField,
            "Item width", widthField,
            "Item height", heightField
        };
        
        int option = JOptionPane.showOptionDialog(
    		null, //Parent component (for location ish)
    		options, 
    		messageTitle, 
    		JOptionPane.OK_CANCEL_OPTION,
    		JOptionPane.QUESTION_MESSAGE, 
            null, //Icon 
            new Object[]{confirmButton, cancelButton}, 
            confirmButton
		);
    
        if (option == JOptionPane.OK_OPTION) {
            String type = typesField.getSelectedItem().toString();
            int  startX = Integer.parseInt(startXField.getText());
            int  startY = Integer.parseInt(startYField.getText());
            int   width = Integer.parseInt( widthField.getText());
            int  height = Integer.parseInt(heightField.getText());
            
            if(selectedID != 0) {
        		commandStack.doCommand( new CommandEditFigure(drawerModel, selectedFigure.getID(), new BaseItem(type, startX, startY, width, height)) );
            } else {
            	Group parentGroup = drawerModel.getGroupAtLocation((int) endPoint.getX(), (int) endPoint.getY());
        		commandStack.doCommand( new CommandAddFigure(drawerModel, new BaseItem(type, startX, startY, width, height), parentGroup.getID() ) );
        	}
    	}
    }

    /**
     * 
     * @param selectedID
     * @param mode True=new Ornament, false=Edit ornament
     */
    public void menuEditAddText(final int selectedID, final boolean mode){
    	DEBUG.Print("Menu Edit AddText (int) opened");
    	
    	final JTextField textField = new JTextField();
		final String[] placesList = {"Top", "Bottom", "Left", "Right"};
		final JComboBox<String> placesField = new JComboBox<String>(placesList);
        final JButton confirmButton = new JButton("Ok");
        final JButton cancelButton = new JButton("Cancel");

        Figure selectedFigure = getModel().getItemAtLocation(selectedID);
		String messageTitle;
		
		DocumentListener isValidDL = new DocumentListener(){
			@Override public void changedUpdate(DocumentEvent de) { validate(de); }
			@Override public void  insertUpdate(DocumentEvent de) { validate(de); }
			@Override public void  removeUpdate(DocumentEvent de) { validate(de); }
			
			public void validate(DocumentEvent e){
                confirmButton.setEnabled( !textField.getText().isEmpty() && Ornament.isValidPosition( placesField.getSelectedItem().toString().toLowerCase() ) );
			}
		};
		textField.getDocument().addDocumentListener(isValidDL);
		((JTextField) placesField.getEditor().getEditorComponent()).getDocument().addDocumentListener(isValidDL);
		
        //Used for allowing user to submit the optionpane or not
        confirmButton.addActionListener(new ActionListener() {
            @Override public void actionPerformed(ActionEvent e) {
                JOptionPane pane = getOptionPane((JComponent)e.getSource());
                pane.setValue(confirmButton);
            }
        });
        cancelButton.addActionListener(new ActionListener() {
            @Override public void actionPerformed(ActionEvent e) {
                JOptionPane pane = getOptionPane((JComponent)e.getSource());
                pane.setValue(cancelButton);
            }
        });
        
        if(mode) { //New Ornament
    		messageTitle = "Add text to this " + selectedFigure.getClass().getSimpleName();
        } else {
        	messageTitle = "Edit this Ornament";
        	textField.setText( ((Ornament) selectedFigure).getCaption() );
        	placesField.setSelectedItem( ((Ornament) selectedFigure).getPosition().toString() );
    	}
		isValidDL.changedUpdate(null);

        //prepare JOptionPane
        Object[] options = {
            "Text", textField,
            "Position", placesField
        };
        
        int option = JOptionPane.showOptionDialog(
    		null, //Parent component (for location ish)
    		options, 
    		messageTitle, 
    		JOptionPane.OK_CANCEL_OPTION,
    		JOptionPane.QUESTION_MESSAGE, 
            null, //Icon 
            new Object[]{confirmButton, cancelButton}, 
            confirmButton
		);
    
        if (option == JOptionPane.OK_OPTION) {
        	if(mode){
        		commandStack.doCommand(new CommandAddText(getModel(), selectedFigure, textField.getText(), placesField.getSelectedItem().toString()));
        	} else {
        		((Ornament) selectedFigure).setCaption( textField.getText() );
        		((Ornament) selectedFigure).setPosition( placesField.getSelectedItem().toString() );
        		commandStack.doCommand(new CommandEditFigure(getModel(), selectedID, selectedFigure));
        	}
        }
	
    }
    
    public void menuEditMenu() {
        DEBUG.Print("Menu EditMenu () opened");
        final int defaultX = drawerPanel.getWidth() / 3;
        final int defaultY = drawerPanel.getHeight() / 3;
        menuEditMenu(defaultX, defaultY);
    }

    public void menuEditMenu(final ArrayList<Integer> selectedIDs) {
        DEBUG.Print("Menu EditMenu (int) opened");
        menuEditMenu(selectedIDs, 0, 0, 0, 0);
    }

    public void menuEditMenu(final int clickX, final int clickY) {
        DEBUG.Print("Menu EditMenu (int, int) opened");
        menuEditMenu(new ArrayList<>( Arrays.asList(0) ), clickX, clickY, 0, 0);
    }

    public void menuEditMenu(final ArrayList<Integer> selectedIDs, final int clickX, final int clickY) {
        DEBUG.Print("Menu EditMenu (int, int) opened");
        menuEditMenu(selectedIDs, clickX, clickY, 0, 0);
    }

    public void menuEditMenu(final Rectangle r) {
        DEBUG.Print("Menu EditMenu (Rectangle) opened");
        menuEditMenu(new ArrayList<>( Arrays.asList(0) ), (int) r.getX(), (int) r.getY(), (int) r.getWidth(), (int) r.getHeight());
    }

    private void menuEditMenu(final ArrayList<Integer> selectedIDs, final int clickX, final int clickY, final int clickW, final int clickH) {
        ArrayList<Figure> selectedFigs = new ArrayList<>();
        for(int ID : selectedIDs){
            Figure fig = getModel().getItemAtLocation(ID);
            if( fig != null ){
                selectedFigs.add(fig);
            }
        }

        final JButton newItemButton     = new JButton("Create a new Item here");
        final JButton cancelButton      = new JButton("Cancel");
        final JButton editItemButton    = new JButton();
        final JButton makeGroupButton   = new JButton();
        final JButton removeFromGroupButton = new JButton();
        final JButton undoGroupButton   = new JButton();
        final JButton addTextButton     = new JButton();
        final JButton deleteItemButton  = new JButton();

        if(selectedFigs.size() == 1){
            if(selectedFigs.get(0) instanceof Group ){
            	undoGroupButton.setText("Disband this group of " + ((Group) selectedFigs.get(0)).getFigures().size() + " items");
            } else if(selectedFigs.get(0) instanceof BaseItem || selectedFigs.get(0) instanceof Ornament){
                editItemButton.setText("Edit this " 		+ selectedFigs.get(0).getClass().getSimpleName());
            }
            removeFromGroupButton.setText("Remove this " 	+ selectedFigs.get(0).getClass().getSimpleName() + " from its group" );
            addTextButton     .setText("Add text to this "	+ selectedFigs.get(0).getClass().getSimpleName());
            deleteItemButton  .setText("Delete this " 		+ selectedFigs.get(0).getClass().getSimpleName());
            
        } else if( selectedFigs.size() > 1){
            makeGroupButton   .setText("Make a group of these items");
            deleteItemButton  .setText("Delete these items");
        } else if( selectedFigs.size() <= 0){
            //only newItemButton
        }

        newItemButton.addActionListener(new ActionListener() {
            @Override public void actionPerformed(ActionEvent e) {
                JOptionPane pane = getOptionPane((JComponent)e.getSource());
                pane.setValue(JOptionPane.OK_OPTION);
                menuEditEditItem(clickX, clickY, clickW, clickH, null);
            }
        });
        
        editItemButton.addActionListener(new ActionListener() {
            @Override public void actionPerformed(ActionEvent e) {
                JOptionPane pane = getOptionPane((JComponent)e.getSource());
                pane.setValue(JOptionPane.OK_OPTION);
                if( selectedFigs.size() == 1 && selectedFigs.get(0) instanceof Ornament ){
                	menuEditAddText(selectedFigs.get(0).getID(), false);
                } else {
                	menuEditEditItem(selectedIDs);
                }
            }
        });

        makeGroupButton.addActionListener(new ActionListener() {
            @Override public void actionPerformed(ActionEvent e) {
                JOptionPane pane = getOptionPane((JComponent)e.getSource());
                pane.setValue(JOptionPane.OK_OPTION);
                commandStack.doCommand(new CommandAddGroup(getModel(), null, selectedIDs, getModel().getGroupAtLocation(clickX, clickY)));
                //TODO NICE Warning if not all items are groupable (i.e. not members of same group)
            }
        });

        ActionListener undoGroupListener = new ActionListener() {
            @Override public void actionPerformed(ActionEvent e) {
                JOptionPane pane = getOptionPane((JComponent)e.getSource());
                pane.setValue(JOptionPane.OK_OPTION);
                commandStack.doCommand(new CommandUndoGroup(getModel(), selectedFigs.get(0) ));
            }
        }; //these buttons do the same, just have different text.
        removeFromGroupButton.addActionListener( undoGroupListener );
        undoGroupButton.addActionListener( undoGroupListener );

        addTextButton.addActionListener(new ActionListener() {
            @Override public void actionPerformed(ActionEvent e) {
                JOptionPane pane = getOptionPane((JComponent)e.getSource());
                pane.setValue(JOptionPane.OK_OPTION);
                menuEditAddText( selectedFigs.get(0).getID(), true );
            }
        });

        deleteItemButton.addActionListener(new ActionListener() {
            @Override public void actionPerformed(ActionEvent e) {
                JOptionPane pane = getOptionPane((JComponent)e.getSource());
                pane.setValue(JOptionPane.OK_OPTION);
                commandStack.doCommand( new CommandRemoveFigure(getModel(), selectedIDs));
            }
        });

        cancelButton.addActionListener(new ActionListener() {
            @Override public void actionPerformed(ActionEvent e) {
                JOptionPane pane = getOptionPane((JComponent)e.getSource());
                pane.setValue(JOptionPane.CANCEL_OPTION);
            }
        });

        ArrayList<Object> options = new ArrayList<>();
        if(selectedFigs.size() == 1){
            options.add(addTextButton);
            if( selectedFigs.get(0) instanceof Group){
                options.add(undoGroupButton);
            } else {
        		options.add(editItemButton);
                if( this.getModel().getParentOfFigure( selectedFigs.get(0).getID() ).getID() != 0 ) {
                	options.add(removeFromGroupButton);
                } //only add this option if the figure is not already in the top-most group (i.e. the model itself).
            }
            options.add(deleteItemButton);
        } else if( selectedFigs.size() > 1){
            options.add(makeGroupButton);
            options.add(deleteItemButton);
        } else if( selectedFigs.size() <= 0){
            options.add(newItemButton);
        }

        JOptionPane.showOptionDialog(
            null, //Parent component (for location ish)
            options.toArray(),
            "Edit Menu",
            JOptionPane.OK_CANCEL_OPTION,
            JOptionPane.QUESTION_MESSAGE,
            null, //Icon
            new Object[]{cancelButton},
            cancelButton
        );

    }

    public void menuEditRemoveItem(final ArrayList<Integer> selectedIDs) {
    	DEBUG.Print("Menu Remove Item opened");
    	commandStack.doCommand( new CommandRemoveFigure(drawerModel, selectedIDs ) );
    }
    
    public void menuEditUndo() {
    	DEBUG.Print("Menu Undo opened");
    	commandStack.undoCommand();
    	this.onNotify();
    }
    
    public void menuEditRedo() {
    	DEBUG.Print("Menu Redo opened");
    	commandStack.redoCommand();
    	this.onNotify();
    }
    
    public void menuViewRefresh(){
        setText("Refreshing!");
        DEBUG.Print(getModel().toString());
    	this.onNotify();
    }
    
    public void menuAboutAbout(){
    	JOptionPane.showMessageDialog(null, 
            "This program was created by Kevin Nauta, for the course Design Patterns (2).\n"+
            "<html>Contact us at <a href='mailto:naut1501@student.nhl.nl'>naut1501@student.nhl.nl</a></html>\n"+
            "(C) 2017", "DesignPatternsDrawer v0.6", JOptionPane.PLAIN_MESSAGE
        );
    }

    //TODO update shortcuts msg
    public void menuAboutShortcuts(){
    	JOptionPane.showMessageDialog(null, 
            "CTRL + N: Create new graph\n"+
            "CTRL + O: Open graph from file \n"+
            "CTRL + S: Save graph\n"+
            "CTRL + SHIFT + S: Save graph as...\n\n"+

            "CTRL + I: Add item\n"+
            "CTRL + E: Edit selected figure\n"+
            "CTRL + SHIFT + I: Remove item\n"+
            "DELETE: Remove item\n"+
//          "CTRL + SHIFT + E: Remove text from selected figure\n\n"+

//          "CTRL + R: Rename item\n"+
            "CTRL + Z: Undo last action\n"+
            "CTRL + Y: Redo next action\n"+
            "CTRL + SHIFT + Z: Redo next action\n"+
//            "CTRL + F: Search item\n"+
            "\n"+
            "Ctrl + Q: Exit\n"+
            "F5: Refresh\n"+
            "F10: Open 'File' menu",
            
            "Shortcuts!", JOptionPane.PLAIN_MESSAGE
        );
    }

    /**
     * Whenever a user uses a keyboard shortcut, this function handles the keypress.
     * If a certain shortcut is registered, the accompanying function is executed.
     * @param e The key being pressed
     */
    @Override public void keyPressed(KeyEvent e) { 
    	//TODO update shortcuts press

        ArrayList<Integer> ignoreables = new ArrayList<>( Arrays.asList( //META == Apple CMD key
                KeyEvent.VK_ENTER, KeyEvent.VK_ESCAPE, KeyEvent.VK_META, KeyEvent.VK_UNDEFINED
        ) );

        control:
        if(e.isControlDown()){
            if(e.isShiftDown()){
                // CTRL + SHIFT + <KEY>
                if(e.getKeyCode() == KeyEvent.VK_S){
                    menuFileSaveAs();
                    break control;
                } else if(e.getKeyCode() == KeyEvent.VK_I && !getModel().isSelected(0) ){
                    menuEditRemoveItem(getModel().getSelectedIDs());
                    break control;
                } else if(e.getKeyCode() == KeyEvent.VK_V /* && selectedID != 0 */){
                    //menuRemoveVertex();
                    break control;
                } else if(e.getKeyCode() == KeyEvent.VK_Z && commandStack.canRedo() ){
                    menuEditRedo();
                }
            
                // CTRL + <KEY>
            } else if(e.getKeyCode() == KeyEvent.VK_N){ //new model
                menuFileNew();
            } else if(e.getKeyCode() == KeyEvent.VK_S){ //save model
                menuFileSave();
            } else if(e.getKeyCode() == KeyEvent.VK_O){ //open from file
                menuFileOpen();
            } else if(e.getKeyCode() == KeyEvent.VK_I){ //new item
                menuEditNewItem();
            } else if(e.getKeyCode() == KeyEvent.VK_Q){ //exit program
                menuFileExit();
            } else if(e.getKeyCode() == KeyEvent.VK_E && ! getModel().isSelected( 0) ){ //edit selected item
                menuEditEditItem(getModel().getSelectedIDs());
            } else if(e.getKeyCode() == KeyEvent.VK_Y && commandStack.canRedo() ){
                menuEditRedo();
            } else if(e.getKeyCode() == KeyEvent.VK_Z && commandStack.canUndo() ){
                menuEditUndo();
            }
            
        // <KEY>
        } else if(e.getKeyCode() == KeyEvent.VK_DELETE && !getModel().isSelected(0) ){ //remove selected item
            menuEditRemoveItem(getModel().getSelectedIDs());
        } else if(e.getKeyCode() == KeyEvent.VK_F5){
            menuViewRefresh();
        
        // IGNORABLE KEYS
        } else if(
    		e.isShiftDown() || 
    		e.isAltDown() ||
            ignoreables.contains(e.getKeyCode())
		) {
        	//ignore
        	
        } else {
            setText("MISPRESSED KEY: " + e.getKeyChar() + " (char: " + e.getKeyCode() + ")");
        }
    }
    @Override public void keyReleased(KeyEvent e) { /* Empty: do nothing */ }
    @Override public void keyTyped(KeyEvent e) { /* Empty: do nothing */ }

}
