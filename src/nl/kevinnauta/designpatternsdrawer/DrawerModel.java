package nl.kevinnauta.designpatternsdrawer;

import java.awt.Rectangle;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * This class represents the file
 */
public class DrawerModel extends Group {
    
	public DrawerModel(DrawerFrame newListener) {
        super();
        this.maxID = 0;
        this.listener = newListener;
        this.filepath = "";
        this.editStatus = false;
        this.selectedIDs = new ArrayList<Integer>();
    }
    
    private DrawerFrame listener;
    public DrawerFrame getListener() { return this.listener; }

    protected int maxID;
    private void setMaxID(int newID) { this.maxID = newID; }
    public int getNextID() { maxID++; return maxID; }

    private ArrayList<Integer> selectedIDs;

    public ArrayList<Integer> getSelectedIDs() {
		ArrayList<Integer> tmp = new ArrayList<Integer>();
		for(Integer i : this.selectedIDs ){
			tmp.add(i);
		}
    	return tmp;
	}

	public boolean isSelected( final int testID ){
    	return selectedIDs.contains(testID);
	}

	public boolean hasAnySelected(){
		return this.selectedIDs.size() > 0 && !this.selectedIDs.contains(0);
	}

	/**
	 * Function that resets the selectedID's to only the specified ID.
	 * @param newID
	 */
	public void selectID( final int newID) {
    	this.selectedIDs.clear();
    	this.selectedIDs.add(newID);
		DEBUG.Print("Selected ID: " + newID);
    	listener.onNotify();
	}

	public void selectIDs( final ArrayList<Integer> newIDs ){
		this.selectedIDs.clear();
		this.selectedIDs.addAll(newIDs);
		DEBUG.Print("Selected IDs: " + newIDs.toString());
		listener.onNotify();
	}

	/**
	 * Function that removes the selection of the current ID
	 * @param oldID
	 */
	public void deselectID( final int oldID ){
    	this.selectedIDs.remove( this.selectedIDs.indexOf(oldID) );
		DEBUG.Print("Deselected ID: " + oldID);
    	listener.onNotify();
	}

	public void deselectIDs( final ArrayList<Integer> oldIDs ){
		this.selectedIDs.removeAll( oldIDs );
		DEBUG.Print("Deselected IDs: " + oldIDs);
		listener.onNotify();
	}

	/**
	 * Function that adds this ID to the set of ID's currently selected.
	 * @param addID
	 */
	public void addSelectID( final int addID ){
		this.selectedIDs.add(addID);
		DEBUG.Print("Selected ID: " + addID);
		listener.onNotify();
	}

	/**
	 * Function that adds all members of the given array to selectedIDs.
	 * @param addIDs
	 */
	public void addSelectIDs( final ArrayList<Integer> addIDs ){
		this.selectedIDs.addAll(addIDs);
		DEBUG.Print("Selected IDs: " + addIDs.toString());
		listener.onNotify();
	}

    public Rectangle drawGroup;

    private String filepath;
    public String getFilePath() { return filepath; }
    public void setFilePath( String newPath ) { this.filepath = newPath; }

    private Boolean editStatus;
    public Boolean isEdited() { return editStatus; }
    public void setEdited(Boolean newEditStatus) {
        this.editStatus = newEditStatus;
        listener.onNotify();
    }
    
    public void placeItemAtLocation(Figure placeFigure, int oldID) {
    	placeFigure.setID(oldID); //give the old ID back to the figure
    	this.placeFigureAtLocation(placeFigure, oldID);
    	this.setEdited(true);
    	DEBUG.Print("Placing this figure at its location: " + oldID + " | " + placeFigure.toString(0) );
    	listener.setText("Placing this " + placeFigure.getClass().getSimpleName() + " with ID " + oldID + " at its old location.");
    }

    public void addNewItem(Figure newFigure, int groupID) {
    	newFigure.setID( this.getNextID() );
    	DEBUG.Print("MDL:70 Adding new object: ID = " + newFigure.getID() + " " + newFigure.toString(0)  );
    	//listener.setText("MDL:71 Adding this new object: " + newFigure.toString(0));
    	
        this.addFigure(newFigure, groupID);
        this.selectID( newFigure.getID() );
        this.setEdited(true);
    }
    
    public void removeItem(int removeID) {
    	listener.setText("Removing Figure with ID:" + removeID + " from the model.");
    	this.deselectIDs( new ArrayList<Integer>( Arrays.asList(removeID)) );
    	this.removeFigure(removeID);
    	this.setEdited(true);
    }
    
    public void setFromClone(DrawerModel clone){
    	this.listener = clone.getListener();
		this.setID( clone.getID() );
		this.selectIDs( clone.getSelectedIDs() );
		this.maxID = clone.maxID;
		this.setFilePath( clone.getFilePath() );
		this.setEdited( clone.isEdited());

		this.setFigures( new ArrayList<Figure>() );
		for(Figure fig : clone.getFigures()){
			this.addFigure(fig, this.getID());
		}
    }
    
    /**
     * Load a file into memory
     * @param filename The location of the file to load into memory.
     * @see Figure
     */
    public void load(String filename) {
    	BufferedReader br = null;
		FileReader fr = null;
		String line;
		String[] split;
		ArrayList<String> file = new ArrayList<String>();

		DEBUG.Print("Started loading of " + filename + ".");
		try {
			fr = new FileReader(filename);
			br = new BufferedReader(fr);

			while ((line = br.readLine()) != null) {
				line = line.replaceAll("\t", ""); //remove all tabs
				file.add(line);
			}
			
			line = file.get(0);
			file.remove(0);
			split = line.split(" ");
			assert(split[0] == "group"); //always start with *this* group
			    					
			file = this.loadFromString(file, Integer.parseInt(split[1]), 0);
			this.selectID(this.getID() - 1);
			this.setMaxID( this.getSelectedIDs().get(0) );
			this.setID(0);

		} catch (IOException e) {
			listener.setText("ERROR: Something went wrong while reading from " + filename + "!");
			e.printStackTrace();

		} finally {
			try {
				if (br != null) { br.close(); }
				if (fr != null) { fr.close(); }
			} catch (IOException ex) {
				listener.setText("ERROR: Something went wrong AFTER reading from " + filename + "!");
				ex.printStackTrace();
			}
		}

		this.filepath = filename;
        this.editStatus = false;
		DEBUG.Print("Loaded model: " + this.filepath + "\n" + this.toString());
		listener.setText("Loaded model from " + this.filepath);
		listener.onNotify();
    }

    /**
     * Save the current Group to a file
     * @see Figure
     */
    public void save(){
    	BufferedWriter bw = null;
		FileWriter fw = null;

		try {
			fw = new FileWriter(this.filepath);
			bw = new BufferedWriter(fw);

			boolean old = DEBUG.ID; DEBUG.save(false);
			bw.write(toString(0));
			DEBUG.save(old);
			
	    	this.editStatus = false;
	    	listener.setText("Saved to file: " + filepath + "!");

		} catch (IOException e) {
			this.editStatus = true;
			listener.setText("ERROR: Something went wrong while saving to " + filepath + "!");
			e.printStackTrace();

		} finally {
			try {
				if (bw != null) { bw.close(); }
				if (fw != null) { fw.close(); }
			} catch (IOException ex) {
				listener.setText("ERROR: Something went wrong AFTER saving to " + filepath + "!");
				ex.printStackTrace();
			}
		}
		DEBUG.Print("Saved to " + filepath + ": " + this.toString(0));
    }

    public String toString() {
    	return super.toString(0) + (DEBUG.ID ? "\n\tSelectedID = " + this.getSelectedIDs() : "");
    }

}