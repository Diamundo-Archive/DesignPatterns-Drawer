package nl.kevinnauta.designpatternsdrawer;

import java.awt.*;
import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;

public class Ornament extends Figure implements DrawableItem {
    public enum POSITION { //type is in caps, so it doesn't interfere with the variable Position
        TOP, BOTTOM, LEFT, RIGHT
    };

    private POSITION Position;
    public  POSITION getPosition() { return Position; }
    public  void setPosition( String newPosition ) {
        switch( newPosition.toLowerCase() ) {
            case "top"    : this.Position = POSITION.TOP;    break;
            case "bottom" : this.Position = POSITION.BOTTOM; break;
            case "left"   : this.Position = POSITION.LEFT;   break;
            case "right"  : this.Position = POSITION.RIGHT;  break;
            default :
                System.err.println("INVALID POSITION ENTERED: " + newPosition + "!");
        }
    }
    /**
     * Quick Function to test if a given String is a valid parseable option for POSITION
     * @param testPosition String to test if it's a valid parseable option for POSITION
     * @return True, if the given string can be parsed to a valid POSITION.
     */
    public static boolean isValidPosition( String testPosition ) {
    	return Arrays.asList( new String[] {"top", "bottom", "left", "right"}).contains(testPosition);
    }
    
    private String Caption;
    public  String getCaption() { return Caption; }
    public  void   setCaption(String newCaption) { this.Caption = newCaption; }

    private Figure Figure;
    public  Figure getFigure() { return Figure; }
    public  void   setFigure(Figure newFigure) { this.Figure = newFigure; }

    public Ornament() { /* empty constructor */ }
    
    public Ornament(String newPosition, String newCaption, Figure newFigure) {
        this.setPosition(newPosition);
        this.Caption = newCaption;
        this.Figure = newFigure;
    }
    
    public ArrayList<String> loadFromString(ArrayList<String> file, String newPosition, String newCaption, int ID) {
		this.setPosition(newPosition);
		this.setCaption(newCaption);
		
    	Iterator<String> lines = file.iterator();
    	String line = "";
    	String[] split;
    	Figure newFig;
    	
    	line = lines.next();
    	lines.remove();
    	split = line.split(" ");
    	
    	switch( split[0]) {
    		case "rectangle" :
    		case "ellipse" :
    			this.setFigure( new BaseItem( line ).setID(ID));
    			this.setID( this.getFigure().getID() + 1);
				break;
    		case "ornament" :
    			newFig = new Ornament();
    			file = ((Ornament) newFig).loadFromString(file, split[1], split[2], ID);
    			this.setFigure(newFig);
    			this.setID( this.getFigure().getID() + 1);
    			break;
    		case "group" : 
    			newFig = new Group();
    			file = ((Group) newFig).loadFromString(file, Integer.parseInt(split[1]), ID);
    			this.setID(newFig.getID());
    			this.setFigure(newFig.setID(ID));
    			break;
    	}

		return file;
    }
    
    public void placeFigureAtLocation(Figure placeFigure, int oldID) {
		if (getFigure().getID() == oldID) {
			this.Figure = placeFigure;
		} else if( getFigure() instanceof Group ) {
			((Group) getFigure()).placeFigureAtLocation(placeFigure, oldID);
		} else if( getFigure() instanceof Ornament ) {
			((Ornament) getFigure()).placeFigureAtLocation(placeFigure, oldID);
		}
	}
    
	public Figure getItemAtLocation(int figureID) {
		Figure foundItem = null;
		
		if( getFigure().getID() == figureID ) {
			foundItem = getFigure();
		} else if( getFigure() instanceof Group ) {
			foundItem = ((Group) getFigure()).getItemAtLocation(figureID);
		} else if( getFigure() instanceof Ornament ) {
			foundItem = ((Ornament) getFigure()).getItemAtLocation(figureID);
		}
		
		return foundItem;
	}
    
	public Figure getItemAtLocation(int x, int y){
    	Figure foundItem = null;
    	
		if( getFigure().getSize().contains(new Point(x,y)) ) {
			if( getFigure() instanceof Ornament ) {
				foundItem = ((Ornament) getFigure()).getItemAtLocation(x, y);
			} else {
				foundItem = getFigure();
			}
		} else if ( this.getSize().contains(new Point(x,y) ) ) {
			foundItem = this; //Figure isn't clicked, but this ornament is clicked (the space that is Ornament, but not Figure)
		} else if ( getFigure()  instanceof Ornament ) {
			foundItem = ((Ornament) getFigure() ).getItemAtLocation(x, y);
		} else if ( getFigure()  instanceof Group) {
			foundItem = ((Group) getFigure() ).getItemAtLocation(x, y);
		}
    	return foundItem;
    }

    public Figure getParentOfFigure(int childID){
    	
    	if(getFigure().getID() == childID){
			return this;
		} else if( getFigure() instanceof Group) {
			return ((Group) getFigure()).getParentOfFigure( childID );
		} else if( getFigure() instanceof Ornament ) {
			return ((Ornament) getFigure()).getParentOfFigure( childID );
		}
    	return null;
	}

    public boolean containsSelectedFigure( final ArrayList<Integer> selectedIDs ) {
		if(getFigure() instanceof BaseItem && selectedIDs.contains(getFigure().getID())){
			return true;
		} else if( getFigure() instanceof Group ) {
			return ((Group) getFigure()).containsSelectedFigure(selectedIDs);
		} else if( getFigure() instanceof Ornament ) {
			return ((Ornament) getFigure()).containsSelectedFigure(selectedIDs);
		}
		return false;
	}

    public String toString(int tabs){
    	return tabs(tabs) + "ornament "  + (DEBUG.ID ? "(" + this.getID() + ") " : "") + Position.toString().toLowerCase() + " " + Caption + "\n" 
			+ getFigure().toString(tabs);
    }

    @Override
    public Rectangle getSize() {
    	Rectangle r = getFigure().getSize();
    	
    	switch(getPosition()) {
	    	case TOP:
	    		r.setLocation( (int) r.getX(), (int) (r.getY() - DrawerPanel.stringHeight - DrawerPanel.stringMargin));
	    		r.setSize( (int) r.getWidth(), (int) (r.getHeight() + DrawerPanel.stringHeight + DrawerPanel.stringMargin));
	    		break;
	    	case BOTTOM:
	    		r.setSize( (int) r.getWidth(), (int) (r.getHeight() + DrawerPanel.stringHeight + DrawerPanel.stringMargin));
	    		break;
	    	case LEFT:
	    		r.setLocation( (int) (r.getX() - DrawerPanel.stringHeight - DrawerPanel.stringMargin), (int) r.getY() );
	    		r.setSize( (int) (r.getWidth() + DrawerPanel.stringHeight + DrawerPanel.stringMargin), (int) r.getHeight()); 
	    		break;
	    	case RIGHT:
	    		r.setSize( (int) (r.getWidth() + DrawerPanel.stringHeight + DrawerPanel.stringMargin), (int) r.getHeight());
	    		break;
    	}
    	
    	return r;
    }
    
}
