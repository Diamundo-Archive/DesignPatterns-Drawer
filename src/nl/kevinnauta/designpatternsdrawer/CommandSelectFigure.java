package nl.kevinnauta.designpatternsdrawer;

import java.util.ArrayList;

public class CommandSelectFigure implements Command {
	@Override
	public String getDescription() { return "Select Figure " + selectedID + " (Mode:" + mode.toString() + ")."; }
	
	public enum MODE {
		ADD, SET, REMOVE
	}
	
	private DrawerModel model; //pointer
	private int selectedID;
	private ArrayList<Integer> previousID;
	private MODE mode;
	
	public CommandSelectFigure( DrawerModel model, int selectedID, MODE mode) {
		this.model = model;
		this.selectedID = selectedID;
		this.mode = mode;
	}
	
	@Override
	public void execute() {
		this.previousID = model.getSelectedIDs();

		switch(this.mode){
			case ADD:
				model.addSelectID(selectedID);
				break;
			case SET:
				model.selectID(selectedID);
				break;
			case REMOVE:
				model.deselectID(selectedID);
		}
	}

	@Override
	public void revive() {
		model.selectIDs( previousID );
		
	}

}
