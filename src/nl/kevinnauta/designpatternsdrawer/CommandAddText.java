package nl.kevinnauta.designpatternsdrawer;

public class CommandAddText implements Command {
	@Override
	public String getDescription() { return "Add '" + addText + "'@'" + addPosition + "' to Figure " + figure.getID() + "."; }
	
	private DrawerModel model; //pointer
	private DrawerModel snapshot;
	private Figure figure;
	private String addText;
	private String addPosition;
	
	public CommandAddText(DrawerModel model, Figure figure, String newText, String newPosition) {
		this.snapshot = new DrawerModel(model.getListener());
		this.model = model;
		this.figure = figure;
		this.addText = newText;
		this.addPosition = newPosition;
	}
	
	@Override
	public void execute() {
		snapshot.setFromClone(model);
		
		Ornament tmp = new Ornament();
		tmp.setCaption( addText );
		tmp.setPosition( addPosition );
		tmp.setFigure( figure );
		tmp.setID( model.getNextID() );
		
		model.placeFigureAtLocation( tmp, figure.getID() ); 
	}

	@Override
	public void revive() {
		model.setFromClone(snapshot);
	}

}
