package nl.kevinnauta.designpatternsdrawer;

import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.List;

public class CommandAddGroup implements Command {
	@Override
	public String getDescription() { return "Add Group '" + (newGroupSize != null ? newGroupSize.toString() : selectionIDs.toString())+ "' to group " + parentGroup.getID(); }

	private DrawerModel model; //pointer
	private DrawerModel snapshot;
	private Rectangle newGroupSize;
	private ArrayList<Integer> selectionIDs;
	private Group parentGroup;

	/**
	 * Command that makes a new group out of all items either within a rectangle or that have been ctrl+selected.
	 * Rectangle and Ctrl+Selection are mutually exclusive!
	 * @param model The model.
	 * @param newGroupSize The rectangle
	 * @param selectionIDs The ctrl selection
	 * @param parentGroup The group where all figures are originally in, and where the new group will be created in.
	 */
	public CommandAddGroup(DrawerModel model, Rectangle newGroupSize, ArrayList<Integer> selectionIDs, Group parentGroup) {
		this.snapshot = new DrawerModel(model.getListener());
		this.model = model;
		this.newGroupSize = newGroupSize;
		this.selectionIDs = selectionIDs;
		this.parentGroup = parentGroup;
		assert( newGroupSize != null ^ selectionIDs != null); //Rectangle and List are mutually exclusive.
	}
	
	@Override
	public void execute() {
		snapshot.setFromClone(model);

		model.addNewItem( new Group(), parentGroup.getID()); 
		int newGroupID = model.getSelectedIDs().get(0);
		//after adding a new item, it gets selected immediately (as only item).

		List<Figure> moveList = new ArrayList<>();
		for(Figure fig : parentGroup.getFigures() ){
    		if(	newGroupSize != null){
    			if( newGroupSize.contains(fig.getSize()) ) { //move all items completely contained in the selection, to the new group.
					moveList.add(fig);
				}
			} else if(selectionIDs != null && selectionIDs.size() > 0){
				if(selectionIDs.contains(fig.getID())){
					moveList.add(fig);
				}
			}
    	}

		for(Figure fig : moveList) {
			model.removeItem(fig.getID());
			model.addFigure(fig, newGroupID);
		}
		
	}

	@Override
	public void revive() {
		model.setFromClone(snapshot);
	}

}
