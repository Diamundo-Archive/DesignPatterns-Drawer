package nl.kevinnauta.designpatternsdrawer;

import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;
import java.awt.geom.RoundRectangle2D;
import java.util.ArrayList;
import javax.swing.JPanel;

@SuppressWarnings("serial")
public class DrawerPanel extends JPanel {
	
    private DrawerFrame listener;
    public DrawerFrame getlistener() { return listener; }
    public void setListener(DrawerFrame listener){ this.listener = listener; }

    private Cat mouseHandler;

    public static final Font captionFont = new Font("Courier New", 1, 15);
    public static final int stringMargin = 5;
    public static final int stringHeight = DrawerPanel.captionFont.getSize();
    public static int getStringWidth(String text) { return ((Graphics) g2).getFontMetrics().stringWidth(text); }
    
    private static Graphics2D g2;
    private Image image;
    public Image getImage(){
        if(image == null){
            image = createImage(getSize().width, getSize().height);
            g2 = (Graphics2D) image.getGraphics();
            g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        }
        return image;
    }

    public DrawerPanel(DrawerFrame newListener){
    	this.setSize(800, 600 - 80);
    	this.setPreferredSize( new Dimension( 800, 600 - 80 ));
        this.listener = newListener;

        mouseHandler = new Cat(listener);
        this.addMouseListener(mouseHandler);
        this.addMouseMotionListener(mouseHandler);
        
        repaint();
        setVisible(true);
    }
    
    /**
     * NOBODYTOUCH
     * Responsible for setting the picture in this panel
     * @param g The canvas
     */
    public void paintComponent(Graphics g) {
        g.drawImage(getImage(), 0, 0, null);
    }
    
    /**
     * Function fired when parent-class sends message.
     * @param drawerModel the updated model
     */
    public void onNotify(DrawerModel drawerModel){
        g2.clearRect(0, 0, this.getWidth(), this.getHeight());
        Paint(drawerModel);
    }

    /**
     * Paint function for the model. This only paints the drawGroup (if applicable), and then redirects the drawing of the model itself to Paint (group)
     * @param model
     */
    public void Paint(DrawerModel model){
        if( model.drawGroup != null ){ //Draw the dragged mousegroup
            g2.setStroke(new BasicStroke(1.0f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND, 10.0f, new float[]{10.0f}, 0.0f));
            g2.setPaint(Color.BLUE);
            g2.draw(new RoundRectangle2D.Double( model.drawGroup.x, model.drawGroup.y, model.drawGroup.width, model.drawGroup.height, 10, 10));
            g2.setPaint(Color.BLACK);
        }
        g2.setFont(captionFont);
        Paint(model, model.getSelectedIDs());
    }

    /**
     * Paint function for a given Group
     * @param group
     * @param selectedIDs
     */
    public void Paint(Group group, ArrayList<Integer> selectedIDs){
        g2.setStroke( new BasicStroke( 1.0f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND, 10.0f, new float[]{10.0f}, 0.0f ) );
        g2.setPaint(Color.BLACK);
        if(selectedIDs.contains(group.getID()) ) {
            g2.setPaint(Color.GREEN);
        } else if( group.containsSelectedFigure(selectedIDs) ) { //selectedID is inside this group, somewhere.
            g2.setPaint(Color.RED);
        }

        if( (group.getID() != 0 || DEBUG.PRINTALLGROUPS) && DEBUG.PRINTGROUP  && group.getGroupSize() > 0) { //if empty group, don't draw it.
            g2.draw(new RoundRectangle2D.Double( group.getSize().getX(), group.getSize().getY(), group.getSize().getWidth(), group.getSize().getHeight(), 10, 10));
        }

        g2.setStroke( new BasicStroke() );
        g2.setPaint(Color.BLACK);

        for(Figure fig : group.getFigures() ) {
            switch(fig.getClass().getSimpleName().toLowerCase()){
                case "baseitem":
                    Paint( (BaseItem) fig, selectedIDs);
                    break;
                case "group":
                    Paint( (Group) fig, selectedIDs);
                    break;
                case "ornament":
                    Paint( (Ornament) fig, selectedIDs);
                    break;
            }
        }
    }

    /**
     * Paint function for an ornament
     * @param fig
     * @param selectedIDs
     */
    public void Paint(Ornament fig, ArrayList<Integer> selectedIDs){
        int x=0, y=0;
        AffineTransform at = new AffineTransform(), oldAT = g2.getTransform();
        Rectangle r = fig.getFigure().getSize();

        switch(fig.getPosition()) {
            case TOP:
                x = (int) (r.getX() + (r.getWidth() / 2) - (getStringWidth(fig.getCaption()) / 2));
                y = (int) (r.getY() - stringMargin);
                break;
            case BOTTOM:
                x = (int) (r.getX() + (r.getWidth() / 2) - (getStringWidth(fig.getCaption()) / 2));
                y = (int) (r.getY() + r.getHeight() + stringHeight + stringMargin);
                break;
            case LEFT:
                x = -1 * (int) (r.getY() + (r.getHeight() / 2) + (getStringWidth(fig.getCaption()) / 2));
                y = (int) (r.getX() - stringMargin);
                at.rotate( - Math.PI / 2); //90 deg. CCW
                break;
            case RIGHT:
                x = (int) (r.getY() + (r.getHeight() / 2) - (getStringWidth(fig.getCaption()) / 2));
                y = -1 * (int) (r.getX() + r.getWidth() + stringMargin);
                at.rotate( Math.PI / 2); //90 deg. CW
                break;
        }

        if(selectedIDs.contains(fig.getID())){
        	g2.setPaint(Color.GREEN);
        } else if( selectedIDs.contains( fig.getFigure().getID())){
        	g2.setPaint(Color.RED);
        }
        g2.setTransform( at );
        g2.drawString(fig.getCaption(), x, y);
        g2.setTransform( oldAT );
        g2.setPaint(Color.BLACK);
        
        switch(fig.getFigure().getClass().getSimpleName().toLowerCase()){
            case "baseitem":
                Paint( (BaseItem) fig.getFigure(), selectedIDs);
                break;
            case "group":
                Paint( (Group) fig.getFigure(), selectedIDs );
                break;
            case "ornament":
                Paint( (Ornament) fig.getFigure(), selectedIDs);
                break;
        }

    }

    /**
     * Paint function for a singular baseItem
     * @param fig
     * @param selectedIDs
     */
    public void Paint(BaseItem fig, ArrayList<Integer> selectedIDs){
        g2.setStroke(new BasicStroke(2));

        if( selectedIDs.contains(fig.getID()) ) {
            g2.setPaint(Color.GREEN);
        }
        switch( fig.getType() ) {
            case RECTANGLE:
                g2.draw(new Rectangle2D.Double( fig.getLeft(), fig.getTop(), fig.getWidth(), fig.getHeight() ));
                break;
            case ELLIPSE:
                g2.draw(new Ellipse2D.Double( fig.getLeft(), fig.getTop(), fig.getWidth(), fig.getHeight() ));
                break;
        }
        g2.setStroke(new BasicStroke());
        g2.setPaint(Color.BLACK);

    }

    public void resizeDrawerPanel(int newWidth, int newHeight) {
		this.setSize( new Dimension (newWidth, newHeight) );
    	setPreferredSize(new Dimension(newWidth, newHeight));
    	
    	image = createImage(newWidth, newHeight);
        g2 = (Graphics2D) image.getGraphics();
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
    	
    	revalidate();
    	repaint();
    }


}
