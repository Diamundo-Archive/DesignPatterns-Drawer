package nl.kevinnauta.designpatternsdrawer;

public class CommandOpenFile implements Command {
	@Override
	public String getDescription() { return "Opening file from " + filepath + "."; }

	private DrawerModel model; //pointer
	private DrawerModel snapshot;
	private String filepath;
	
	public CommandOpenFile(DrawerModel model, String filepath) {
		this.snapshot = new DrawerModel(model.getListener());
		this.model = model;
		this.filepath = filepath;
	}
	
	@Override
	public void execute() {
		snapshot.setFromClone(model);
		model.removeFigure(0);
		model.selectID(0);
		model.load(filepath);
	}

	@Override
	public void revive() {
		model.setFromClone( snapshot );
	}

}
