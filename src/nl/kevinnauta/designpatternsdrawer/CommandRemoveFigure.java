package nl.kevinnauta.designpatternsdrawer;

import java.util.ArrayList;

public class CommandRemoveFigure implements Command {
	@Override
	public String getDescription() { return "Remove Figure " + removeFigureIDs.toString() + "."; }
	
	private DrawerModel model; //pointer
	private DrawerModel snapshot;
	private ArrayList<Integer> removeFigureIDs;
	
	public CommandRemoveFigure(DrawerModel model, ArrayList<Integer> removeFigureIDs) {
		this.snapshot = new DrawerModel(model.getListener());
		this.model = model;
		this.removeFigureIDs = removeFigureIDs;
	}
	
	@Override
	public void execute() {
		snapshot.setFromClone(model);
		for(int ID : removeFigureIDs) {
			model.removeItem(ID);
		}
	}

	@Override
	public void revive() {
		model.setFromClone( snapshot );
	}

}
